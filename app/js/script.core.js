;(function($){

	"use strict";

	var Core = {

		DOMReady: function(){

			var self = this;
			
			self.navigation.init();
			self.headerSticky.init();
			self.navResp();
			self.tel.init();
			self.simpleTooltip();
			
		},

		windowLoad: function(){

			var self = this;

			self.preloader();
			self.animatedContent();
			// self.footerBottom.init();
			if($('.gmap').length){

				self.googleMaps();

			}
			if($('.gmap2').length){

				self.googleMaps2();

			}
			
		},

		simpleTooltip :function(){

            function simple_tooltip(target_items, name){
                $(target_items).each(function(i){
	                $("body").append("<div class='"+name+" "+$(this).data("sold")+"' id='"+name+i+"'><p>"+$(this).attr('title')+"</p></div>");
	                var my_tooltip = $("#"+name+i);

	                $(this).removeAttr("title").mouseover(function(){
	               		my_tooltip.css({opacity:0.9, display:"none"}).fadeIn(0);
	                }).mousemove(function(kmouse){
	                	my_tooltip.css({left:kmouse.pageX+25, top:kmouse.pageY-10});
	                }).mouseout(function(){
	                	my_tooltip.fadeOut(0);
	                });
                });
            }
            $(document).ready(function(){
            	simple_tooltip(".tooltip_on","tooltip");
            });

        },

		/**
        **  Animated Content
        **/

        animatedContent : function(){

            $("[data-animation]").each(function() {

                var $this = $(this);

                if($(window).width() > 767) {

                    $this.appear(function() {

                        var delay = ($this.attr("data-animation-delay") ? $this.attr("data-animation-delay") : 1);

                        if(delay > 1) $this.css("animation-delay", delay + "ms");
                        $this.removeClass('transparent').addClass("visible " + $this.attr("data-animation"));   

                    }, {accX: 0, accY: -100});

                }
                else {

                    $this.removeClass("transparent").addClass("visible");

                }

            });

        },

		/**
		**	Header Sticky
		**/

		headerSticky: {

			init: function(){

				var self = this;

					self.header = $('#header');
					self.screen = $('.video_container');
					self.screenOffset = self.screen.offset().top;
					self.w = $(window);
					self.headerStickyH = 78;

				

				self.sticky();

				self.w.on('scroll', function(){

					self.sticky();
					
				});

			},

			sticky: function(){

				var self = this;
					self.wScroll = self.w.scrollTop();
					self.sectionMainH = $('.section_main').outerHeight() - self.headerStickyH;

				if(self.wScroll >0){
					self.header.addClass('sticky');
				}
				else{
					self.header.removeClass('sticky');
				}

				if(self.wScroll >= self.sectionMainH){
					$("#header").removeClass("first_screen_active");
				}
				else{
					$("#header").addClass("first_screen_active");
				}
			},

		},



		/**
		**	Main navigation
		**/

		navigation: {

		    init: function () {

		    	var self = this;
		    	
		    	self.w = $(window);
		    	self.body = $('body');
		    	self.navParent = $('.navigation_menu');
		    	self.nav = $('.navigation');
		    	self.section = $('.section');
		    	self.sectionQt = self.section.length;
		    	self.headerStickyH = 78;

		    	self.anchorScroll();
		    	self.bodyHeaderH();

		    	self.w.on('scroll',function(){

		    		self.pageScroll();

		    	});

		    },

		    bodyHeaderH: function(){

		    	var self = this;
		    	self.headerH = $('#header').outerHeight();

		    	self.nav.on('click', "a", function(event){

		    		event.preventDefault();

		    		var $this = $(this),
		    			item = $this.parent(),
		    			dataId = $this.attr('href'),
		    			offset = $(dataId).offset().top - self.headerStickyH; // - self.headerH;


		    		item.addClass('current').siblings().removeClass('current');

		    		self.scrollContent(offset);

		    	});

		    },

		    anchorScroll: function(){

		    	var self = this;
		    	self.headerH = $('#header').outerHeight();
		    	//self.headerH;

		    	//$(body).attr('style="padding-top: 200px;"');



		    },

		    scrollContent: function(offset){

		    	var self = this;

		    	self.body.addClass('scrollContent');

		    	$('html,body').stop().animate({

					scrollTop: offset

		    	},1000,function(){

		    		self.body.removeClass('scrollContent');

		    	});

		    },

		    pageScroll: function(){

		    	var self = this;

		    	if(self.body.hasClass('scrollContent'))return;

		    	self.wScroll = self.w.scrollTop();
		    	self.wHeightHalf = self.headerStickyH;

		    	for (var i = 0; i <= self.sectionQt - 1; i++) {

		    		var offset = $(self.section[i]).offset().top,
		    			heightBox = $(self.section[i]).outerHeight(),
		    			bottomOffset = $(self.section[i+1]).length ? $(self.section[i+1]).offset().top : offset + heightBox,
		    			id = $(self.section[i]).attr('id'),
		    			activItem = $('.navigation').find("a[href='" + "#" + id + "']").parent();
		    		
		    		//$('.navigation li').removeClass('active');
		    		//$('.navigation_point li').removeClass('navigation_point_active');

		    		if(self.wScroll + self.wHeightHalf > offset && self.wScroll + self.wHeightHalf < bottomOffset ){
		    		
		    			setTimeout(function(){

		    			},1000)

		    			activItem.addClass('current').siblings().removeClass('current');
		    			//$("#header").toggleClass(id);

		    			return false;

		    		}
		    		
		    	};



		    },



		},


		tel: {

			init: function(){

				var self = this;
					self.telephone = $('.js-tel-hide');

				self.changeNumber();

			},

			changeNumber: function(){
				
				var self = this;

				self.telephone.each(function(index, el) {
            		
	            	var telNumber = $(this).text(),
	            	telDel = telNumber.substring( 0, telNumber.length - 2 ),
	            	appendSpan = "<span class='js-wrap-tel' data-number='"+ telNumber +"'></span>",
					appendText = "<span class='js-show_tel'>Показать номер</span><span class='two_dots'></span>";



	            	$(this).text(telDel);
	            	$(this).wrap(appendSpan).closest('.js-wrap-tel').append(appendText);

					
	            	$('.js-show_tel').on('click', function(event) {
						event.preventDefault();
						// self.dataNumb = $(this).closest('.js-wrap-tel').attr('data-number');
						
						// $(this).closest('.js-wrap-tel').find('.js-tel-hide').text(self.dataNumb);
						$(this).closest('.js-wrap-tel').find('.js-tel-hide').text(telNumber);
						$(this).closest('.js-wrap-tel').addClass('active').find('.js-show_tel, .two_dots').remove();

					});
	            });

			}

        },


	   	/**
		**	Responsive menu
		**/

		navResp : function(){

            var self = this;

            self.w = $(window);


            $(".button_nav").on('click', function () {

                $(this).toggleClass("active").next(".header_resp_nav").slideToggle("medium");
          
            });
            

            

           if(self.w.width() < 768){

                $(document).on('click', function(event){

                   if(!$(event.target).closest('.navigation_menu').length){

                    $('.button_nav').removeClass('active').next(".header_resp_nav").slideUp("medium");
                   
                   }

              });

                $(".menu_item").on('click', function () {

	                $(".button_nav").toggleClass("active").next(".header_resp_nav").slideToggle("medium");
	          
	            });

            }

        },

	   	/**
		**	Footer Bottom
		**/
		
		footerBottom: {

			init: function(){

				var self = this;

				self.footer = $('#footer');
				self.page = $('.page_wrap');

				self.calculation();

				$(window).on('resize', function(){

					self.calculation();

				});

			},

			calculation : function(){

				var self = this;
				
			    var footerHeight = self.footer.outerHeight();

			    self.page.css({
			    	'padding-bottom': footerHeight 
			    });

			}

		},


		/**
		**	Google Map
		**/

	    

	    googleMaps: function(){

	        var mapsCollection = [],
	        	mapSettings = {

					zoom: 1,
					scrollwheel: false,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
					generate_controls: false,
					controls_on_map: false,
					view_all: false

			    };

	        $('.gmap1').each(function(i, el){

	            var dataCoords = $(el).data('coords'),
		    		id 		   = $(el).attr('id'),
	                dataZoom   = $(el).data('zoom'),
	                map;

	            mapSettings.zoom = dataZoom;
	            mapSettings.center = dataCoords.center;

				map = new google.maps.Map(document.getElementById(id), mapSettings);

				/* Map center on resize start
				=========================*/
				    var getCen = map.getCenter();

				    google.maps.event.addDomListener(window, 'resize', function() {
				        map.setCenter(getCen);
				    });

			    /* Map center on resize end
			    ================*/

			    for (var i = dataCoords.marker.length - 1; i >= 0; i--) {
			    	
			    	var marker = new google.maps.Marker({
					    position: dataCoords.marker[i].position,
					    map: map,
					    icon: dataCoords.marker[i].icon,
					    title: dataCoords.marker[i].title
					  });

			    }

	        });

	        if(!mapsCollection.length) return;

	        $(window).on('resize.map', function(){
	            setTimeout(function(){
	                mapsCollection.forEach(function(elem, index, arr){
	                    elem.Load();
	                });
	            }, 100);
	        });

	        // <div class="gmap" id="gmap" data-zoom="15" data-type="hybrid" data-coords='[{"lat": 55.547963, "lon": 36.985302, "title":"Кластер"}]'></div>

	    },

	    googleMaps2: function(){

	        var mapsCollection = [];

	        var styleArray ={
	        	'dark': [{"featureType": "all", "elementType": "labels.text.fill", "stylers": [{"saturation": 36 }, {"color": "#000000"}, {"lightness": 40 } ] }, {"featureType": "all", "elementType": "labels.text.stroke", "stylers": [{"visibility": "on"}, {"color": "#000000"}, {"lightness": 16 } ] }, {"featureType": "all", "elementType": "labels.icon", "stylers": [{"visibility": "off"} ] }, {"featureType": "administrative", "elementType": "geometry.fill", "stylers": [{"color": "#000000"}, {"lightness": 20 } ] }, {"featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{"color": "#000000"}, {"lightness": 17 }, {"weight": 1.2 } ] }, {"featureType": "landscape", "elementType": "geometry", "stylers": [{"color": "#000000"}, {"lightness": 20 } ] }, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#000000"}, {"lightness": 21 } ] }, {"featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{"color": "#000000"}, {"lightness": 17 } ] }, {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#000000"}, {"lightness": 29 }, {"weight": 0.2 } ] }, {"featureType": "road.arterial", "elementType": "geometry", "stylers": [{"color": "#000000"}, {"lightness": 18 } ] }, {"featureType": "road.local", "elementType": "geometry", "stylers": [{"color": "#000000"}, {"lightness": 16 } ] }, {"featureType": "transit", "elementType": "geometry", "stylers": [{"color": "#000000"}, {"lightness": 19 } ] }, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#000508"}, {"lightness": 17 } ] } ],
	        	'default': [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
	        };

	        $('.gmap2').each(function(i, el){

	            var dataCoords = $(el).data('coords'),
	                dataZoom = $(el).data('zoom'),
	                dataStyle = $(el).data('style') ? $(el).data('style') : 'default',
	                dataType = $(el).data('type'),
	                mapStyle;

	            for (var key in styleArray) {
				  if(key == dataStyle){
				  	mapStyle = styleArray[key];
				  }
				}
	            var mapSettings = {
	                map_options: {
	                	styles: mapStyle,
	                    zoom: dataZoom,
	                    scrollwheel: false,
	                    mapTypeId: dataType
	                },
	                locations: dataCoords,
	                generate_controls: false,
	                controls_on_map: false,
	                view_all: false,
	                map_div: '#' + $(el).attr('id')
	            };

	            mapsCollection.push(new Maplace(mapSettings).Load());

	        });

	        if(!mapsCollection.length) return;

	        $(window).on('resize.map', function(){
	            setTimeout(function(){
	                mapsCollection.forEach(function(elem, index, arr){
	                    elem.Load();
	                });
	            }, 100);
	        });

	        // <div class="gmap" id="gmap" data-zoom="15" data-type="hybrid" data-coords='[{"lat": 55.547963, "lon": 36.985302, "title":"Кластер"}]'></div>

	    },



		/**
		**	Preloader
		**/

		preloader: function(){

			var self = this;

			self.preloader = $('#page-preloader');
	        self.spinner   = self.preloader.find('.preloader');

		    self.spinner.fadeOut();
		    self.preloader.delay(350).fadeOut('slow');
		},

	}


	$(document).ready(function(){

		Core.DOMReady();

	});

	$(window).load(function(){

		Core.windowLoad();

	});

})(jQuery);