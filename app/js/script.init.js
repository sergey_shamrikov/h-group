//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/





		var video 		= $('.js-video'),
			animation   = $("[data-animation]"),
			slider 		= $('.slider-pro'),
			maxhheight 	= $("[data-mh]"),
			validate   	= $('.validate'),
			mask	   	= $('input[type="tel"]'),
			phonecode	= $('.phonecode'),
		    owl 		= $(".owl-carousel"),
			modal 		= $(".modal_btn"),
			map 	  	= $('.gmap');


		$(document).ready(function(){


			/* ------------------------------------------------
					Video pudin
			------------------------------------------------ */

			var width = $(window).width();
		    if (width >= 768) {
		        if(video.length){

					video.vide({
						//3gp: 'media/video/videoplayback.3gp',
					  	mp4: 'media/video/first_screen.mp4',
						poster: 'media/video/first_screen.png'
					}, 
					{
						volume: 0,
						autoplay: true,
						posterType: 'png', // Poster image type. "detect" — auto-detection; "none" — no poster; "jpg", "png", "gif",... - extensions.
						bgColor: 'rgba(0, 0, 0, .3)', // Allow custom background-color for Vide div,
						className: 'video_box' // Add custom CSS class to Vide div
					});

				}
		    }

			$(window).resize(function() {
			    width = $(window).width();
			    if (width >= 768) {
			        if(video.length){

						video.vide({
							//3gp: 'media/video/videoplayback.3gp',
						  	mp4: 'media/video/first_screen.mp4',
							poster: 'media/video/first_screen.png'
						}, 
						{
							volume: 0,
							autoplay: true,
							posterType: 'png', // Poster image type. "detect" — auto-detection; "none" — no poster; "jpg", "png", "gif",... - extensions.
							bgColor: 'rgba(0, 0, 0, .3)', // Allow custom background-color for Vide div,
							className: 'video_box' // Add custom CSS class to Vide div
						});

					}
			    }
			});

				

	        /* ------------------------------------------------
					End of Video pudin
			------------------------------------------------ */




			/* ------------------------------------------------
			Inputmask START
			------------------------------------------------ */

			if(mask.length){

				mask.mask("+7 (999) 999-99-99");

			}

		 
			/* ------------------------------------------------
			Inputmask END
			------------------------------------------------ */

			/* ------------------------------------------------
			Inputmask START
			------------------------------------------------ */

			if(phonecode.length){

				phonecode.phonecode({
			        preferCo: 'ru'
			    });

			}

						
		 
			/* ------------------------------------------------
			Inputmask END
			------------------------------------------------ */




			/* ------------------------------------------------
			Validate START
			------------------------------------------------ */

			if(validate.length){

					$(".validate").each(function(index,el){
                    var id = $(el).attr("id");

					$("#" + id).validate({

						rules:{

							cf_name: {
								required: true,
								minlength: 2
							},

							cf_email: {
								//required: true,
								email: true
							},

							cf_tel: {
								required: true,
								number:true,
							},

						},

						messages:{

							cf_name: {
								required: "Поле обязательно для заполнения",
								minlength: 'Введите не менее 2 символов.'
							},

							cf_email: {
								required: "Поле обязательно для заполнения",
								email: "Не верный email."
							},

							cf_tel: {
								required: "Поле обязательно для заполнения",
								number: "Неправильно введен номер",
								//inlength: "Введите не менее 10 символов."
							},

						}

					});

				});	

			}

			/* ------------------------------------------------
			Validate END
			------------------------------------------------ */



			/* ------------------------------------------------
			OWL START
			------------------------------------------------ */

					if(owl.length){
						$('.carousel_1').owlCarousel({
							items : 1,
							loop: true,
							smartSpeed:1000,
							nav: true
							// autoHeight:true
						});
					}

					if(owl.length){
						$('.carousel_2').owlCarousel({
							center: true,
							items : 2,
							loop: true,
							smartSpeed:1000,
							nav: true,
							margin: 100,
							navText: [ '', '' ],
							// autoHeight:true
							responsive:{
						        0:{
						            items:1,
						            nav:true
						        },
						        768:{
						            items:2,
						        },
						    }
						});
					}

					// if(owl.length){
					//     owl.each(function(){
					//     	var $this = $(this),
					//       		items = $this.data('items');

					//     	$this.owlCarousel({
					// 			items : 1,
					// 			// loop: true,
					// 			smartSpeed:1000,
					// 			// autoHeight:true,
					//     		dots:false,
					//     		nav: true,
					//             navText: [ '', '' ],
					//             // margin: 30,
					//             responsive : items
					//     	});
					//     });
					// }
					// <div class="owl-carousel" data-items='{  "0":{"items":1},   "480":{"items":2},   "991":{"items":3}  }'></div>

			/* ------------------------------------------------
			OWL END
			------------------------------------------------ */


			/* ------------------------------------------------
			TABS START
			------------------------------------------------ */

					if(slider.length){
						
						$( '.slider_1' ).sliderPro({
							//width: 670,
							//height: 500,
							orientation: 'vertical',
							loop: false,
							arrows: true,
							buttons: false,
							thumbnailsPosition: 'right',
							thumbnailPointer: true,
							thumbnailWidth: 100,
							thumbnailHeight: 100,
							breakpoints: {
								800: {
									thumbnailsPosition: 'bottom',
									thumbnailWidth: 80,
									thumbnailHeight: 80
								},
								500: {
									thumbnailsPosition: 'bottom',
									thumbnailWidth: 50,
									thumbnailHeight: 50
								}
							}
						});


						$( '.slider_modal' ).sliderPro({
							width: 580,
							height: 460,
							autoplay: false,
							autoScaleLayers: false,
							orientation: 'vertical',
							loop: false,
							arrows: true,
							buttons: false,
							thumbnailsPosition: 'right',
							thumbnailPointer: true,
							thumbnailWidth: 120,
							thumbnailHeight: 110,

							breakpoints: {
								800: {
									// width: 580,
									// height: 460,
									responsive:true,
									thumbnailsPosition: 'bottom',
									orientation: 'horizontal',
									thumbnailWidth: 90,
									thumbnailHeight: 110,

								},
								500: {
									responsive:true,
									thumbnailsPosition: 'bottom',
									orientation: 'horizontal',
									thumbnailWidth: 70,
									thumbnailHeight: 100
								}
							}
						});		

					}

			/* ------------------------------------------------
			TABS END
			------------------------------------------------ */


			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(modal.length){
						$(".modal_btn").on("click",function(event){

			                event.preventDefault();

			                var id = $(this).attr("data-modal"),
			                    src = $(this).attr("data-src");

			                $('#'+id).arcticmodal({
			                    
			                    beforeOpen : function(){

			                        $('#'+id).find("iframe").attr("src", src+"?wmode=transparent");

			                    },
			                    afterOpen : function(){

									var slider = $( '.slider_modal' ).data( 'sliderPro' );
			                    	slider.update();
			                    },

			                });

			            });
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */




		});

		
		$(window).load(function(){

			

		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
